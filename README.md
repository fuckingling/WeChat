﻿# 【Wx.Sdk使用方法】[![Fork me on Gitee](https://gitee.com/fuckingling/WeChat/widgets/widget_3.svg)](https://gitee.com/fuckingling/WeChat)


### 【appsettings.json中微信API和PAY的JSON配置】
```json
  "WeChat": {
    /// <summary>
    /// 应用ID
    /// </summary>
    "appid": "",
    /// <summary>
    /// 应用密钥
    /// </summary>
    "appsecret": "",
    /// <summary>
    /// 令牌
    /// </summary>
    "token": "",
    /// <summary>
    /// 消息加解密密钥 目前明文模式 不需要填
    /// </summary>
    "encodingaeskey": "",
    /// <summary>
    /// 消息接收url
    /// </summary>
    "url": "/wx"
  },
    //-------------------------------支付相关------------------------------------
    "Pay": {
  
      /// <summary>
      /// 支付账户的微信ID
      /// </summary>
      "appid": "",
      /// <summary>
      /// 微信支付商户号
      /// </summary>
      "mchid": "",
      /// <summary>
      /// 加密密钥
      /// </summary>
      "sign_key": "",
      /// <summary>
      /// SSL证书目录 注意应该填写绝对路径（仅退款、撤销订单时需要）
      /// </summary>
      "sslpath": "",
      /// <summary>
      /// 证书密码
      /// </summary>
      "sslpassword": "",
      /// <summary>
      /// 支付异步消息回调
      /// </summary>
      "pay_notify": "/wx/pay_notifyurl",
      /// <summary>
      /// 退款异步消息回调
      /// </summary>
      "refund_notify": "/wx/refund_notifyurl",
      /// <summary>
      /// 回调域名
      /// </summary>
      "notify_url": "http://xxxxx"
    }
```
### 【微信API】
#####   Startup.cs的ConfigureServices方法中添加服务
```C#
//微信API配置
services.AddWeChatApi(Configuration.GetSection("WeChat"));
```
#####   如果需要接收消息和被动回复消息则需要下面2步：
- Configure中加入消息中间件：
```C#
//微信消息中间件
app.AddWeChatApiMessageMiddleware();
```
- 创建一个xxx类实现WxMessage并把ConfigureServices中的添加改成：
```C#
//微信API配置
services.AddWeChatApi<xxx>(Configuration.GetSection("WeChat"));
```
- 接收一个消息并被动回复一个消息用例：
```C#
protected override void TextMsg(TextMsg msg)
{
    this.ReplyTextMsg($"你好,您发送的消息是:{msg.Content}");
}
```
#####   微信API的access_token需要缓存机制，SDK公开了一个IWxCache接口，默认实现是MemoryCache，若用户需要用redis等其他缓存则需要创建一个类ccc实现IWxCache接口：
```C#
//微信API配置
services.AddWeChatApiCache<ccc>();
```
#####   SDK已单利注入WxApi，如要其他API接口则调用WxApi中的ApiService即可
```C#
WxApi wxApi;
WxPay wxPay;
public HomeController(WxApi wxApi, WxPay wxPay)
{
    this.wxApi = wxApi;
    this.wxPay = wxPay;
}
public string TestSendTemplateMsg()
{
    wxApi.ApiService.SendTemplateMsg("openid", "模板ID",
                    new Keyword("标题"), new Keyword("备注"),
                    "跳转url", 跳转小程序,
                    new Keyword("1"),
                    new Keyword("2"),
                    new Keyword("3"));
    return "1";
}
```
### 【微信支付】
- #####   创建一个WxPayService类实现Notify
```C#
    public class WxPayService : Notify
    {
        public WxPayService(WxPay wxPay) : base(wxPay)
        {
        }
        protected override bool Pay(PayNotify payNotify)
        {
            LogerHelper.Debug($"成功支付:{payNotify.out_trade_no}");
            return true;
        }

        protected override bool Refund(RefundNotifyInfo refundNotifyInfo)
        {
            LogerHelper.Debug($"成功退款:{refundNotifyInfo.out_refund_no}");
            return true;
        }
    }
```
- #####   Startup.cs的ConfigureServices方法中添加服务
```C#
//微信支付
services.AddWeChatPay<WxPayService>(Configuration.GetSection("Pay"));
```
- #####   Configure中加入回调中间件：
```C#
//支付退款回调中间件
app.AddWeChatPayMiddleware();
```
### 【完整配置】
```C#
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //微信API配置
            services.AddWeChatApi(Configuration.GetSection("WeChat")).AddWeChatApiMessage<WxMessage>().AddWeChatApiCache<WxCache>();
            //微信支付
            services.AddWeChatPay<WxPayService>(Configuration.GetSection("Pay"));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //微信消息中间件
            app.AddWeChatApiMessageMiddleware();
            //支付退款回调中间件
            app.AddWeChatPayMiddleware();

            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
```