﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wx.Sdk.Pay;
using Wx.Sdk.Pay.Entity;
using Wx.Sdk.Pay.Src;

namespace Web.Core
{
    public class WxPayService : Notify
    {
        public WxPayService(WxPay wxPay) : base(wxPay)
        {
        }
        protected override bool Pay(PayNotify payNotify)
        {
            LogerHelper.Debug($"成功支付:{payNotify.out_trade_no}");
            return true;
        }

        protected override bool Refund(RefundNotifyInfo refundNotifyInfo)
        {
            LogerHelper.Debug($"成功退款:{refundNotifyInfo.out_refund_no}");
            return true;
        }
    }
}
