﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wx.Sdk.Api;
using Wx.Sdk.Api.Entity.Messages;
using Wx.Sdk.Api.Entity.Base;
using Wx.Sdk.Api.Src;
using Wx.Sdk.Api.Entity.Template;
using Wx.Sdk;

namespace Web.Core
{
    public class WxApiMessage : WxMessage
    {
        public WxApiMessage(WxApi wxApi) : base(wxApi)
        {
        }

        protected override void ImageMsg(ImageMsg msg)
        {
            throw new NotImplementedException();
        }

        protected override void LinkMsg(LinkMsg msg)
        {
            throw new NotImplementedException();
        }

        protected override void LocationEvent(LocationEvent evt)
        {
            throw new NotImplementedException();
        }

        protected override void LocationMsg(LocationMsg msg)
        {
            throw new NotImplementedException();
        }

        protected override void MenuClickEvent(MenuEvent evt)
        {
            throw new NotImplementedException();
        }

        protected override void MenuViewEvent(MenuEvent evt)
        {
            throw new NotImplementedException();
        }

        protected override void Msg(Msg msg)
        {

        }

        protected override void ScanQRCodeEvent(ScanQRCodeEvent evt)
        {
            throw new NotImplementedException();
        }

        protected override void SubscribeEvent(SubscribeEvent evt)
        {
            throw new NotImplementedException();
        }

        protected override void TemplateMsgEvent(TemplateMsgEvent evt)
        {
            throw new NotImplementedException();
        }

        protected override void TextMsg(TextMsg msg)
        {
            this.ReplyTextMsg("你好");
                wxApi.ApiService.SendTemplateMsg(msg.FromUserName, "AMv8Yqpx0cioJkB24pIjNtt2gRS2kluidvLuegMuohw",
            new Keyword("标题"), new Keyword("备注"),
            "", null,
            new Keyword("1"),
            new Keyword("1"),
            new Keyword("1"));
        }

        protected override void UnSubscribeEvent(UnSubscribeEvent evt)
        {
            throw new NotImplementedException();
        }

        protected override void VideoMsg(VideoMsg msg)
        {
            throw new NotImplementedException();
        }

        protected override void VoiceMsg(VoiceMsg msg)
        {
            throw new NotImplementedException();
        }
    }
}
