﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Common.Standard
*文件名： LogerHelper
*创建人： kingling
*创建时间：2018/9/4 16:31:24
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/4 16:31:24
*修改人：kingling
*描述：
************************************************************************/


using NLog;

namespace Web.Core
{
    public class LogerHelper
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();
        public static void Error(string err)
        {
            logger.Error(err);
        }
        public static void Warn(string err)
        {
            logger.Warn(err);
        }
        public static void Debug(string err)
        {
            logger.Debug(err);
        }
        public static void Info(string err)
        {
            logger.Info(err);
        }
        public static void Fatal(string err)
        {
            logger.Fatal(err);
        }
    }
}
