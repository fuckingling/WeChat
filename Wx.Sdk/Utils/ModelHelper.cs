﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Utils
*文件名： ModelHelper
*创建人： kingling
*创建时间：2018/10/6 18:00:55
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 18:00:55
*修改人：kingling
*描述：
************************************************************************/

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace Wx.Sdk.Utils
{
    internal static class ModelHelper
    {
        /// <summary>
        /// 将对象转换成链接参数 &
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static string ToUrlParameter<T>(T t) where T : class
        {
            var str = "";
            if (t == null)
            {
                return str;
            }
            var list = t.GetType().GetProperties().Where(x => x.GetValue(t) != null && !x.GetValue(t).ToString().IsNull()).OrderBy(x => x.Name).Select(x => $"{x.Name}={x.GetValue(t).ToString()}");
            str = string.Join("&", list);
            return str;
        }
    }
}
