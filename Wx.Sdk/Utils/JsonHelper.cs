﻿using Newtonsoft.Json;

namespace Wx.Sdk.Utils
{
    internal class JsonHelper
    {
        /// <summary>
        /// 将model序列号成json
        /// </summary>
        /// <param name="t">model</param>
        /// <returns></returns>
        public static string ToJson<T>(T t) where T : class
        {
            try
            {
                return JsonConvert.SerializeObject(t);
            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// 将json反序列化成model
        /// </summary>
        /// <param name="str">json</param>
        /// <returns></returns>
        public static T ToModel<T>(string str) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(str);
            }
            catch
            {
                return default(T);
            }
        }
    }
}
