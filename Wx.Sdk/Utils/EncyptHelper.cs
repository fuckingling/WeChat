﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Utils
*文件名： EncyptHelper
*创建人： kingling
*创建时间：2018/9/29 14:51:09
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 14:51:09
*修改人：kingling
*描述：
************************************************************************/

using System;
using System.Security.Cryptography;
using System.Text;

namespace Wx.Sdk.Utils
{
    internal static class EncyptHelper
    {
        /// <summary>
        /// hash_hmac256
        /// </summary>
        /// <param name="txt"></param>
        /// <param name="secretKey"></param>
        /// <returns></returns>
        public static string ToHMACSHA256(string txt, string secretKey)
        {
            var enc = Encoding.UTF8;
            HMACSHA256 hmac = new HMACSHA256(enc.GetBytes(secretKey));
            hmac.Initialize();
            byte[] buffer = enc.GetBytes(txt);
            return BitConverter.ToString(hmac.ComputeHash(buffer)).Replace("-", "");
        }
        /// <summary>
        /// 基于Sha1的自定义加密字符串方法：输入一个字符串，返回一个由40个字符组成的十六进制的哈希散列（字符串）。
        /// </summary>
        /// <param name="str">要加密的字符串</param>
        /// <returns>加密后的十六进制的哈希散列（字符串）</returns>
        public static string ToSha1(string str)
        {
            var buffer = Encoding.UTF8.GetBytes(str);
            var data = SHA1.Create().ComputeHash(buffer);

            var sb = new StringBuilder();
            foreach (var t in data)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }
        /// <summary>
        /// 基于Md5的自定义加密字符串方法：输入一个字符串，返回一个由32个字符组成的十六进制的哈希散列（字符串）。
        /// </summary>
        /// <param name="str"></param>
        /// <param name="lower">是否小写</param>
        /// <returns></returns>
        public static string ToMD5(string str, bool lower = true)
        {
            //将输入字符串转换成字节数组
            var buffer = Encoding.UTF8.GetBytes(str);
            //接着，创建Md5对象进行散列计算
            var data = MD5.Create().ComputeHash(buffer);

            //创建一个新的Stringbuilder收集字节
            var sb = new StringBuilder();

            //遍历每个字节的散列数据 
            foreach (var t in data)
            {
                //格式每一个十六进制字符串
                sb.Append(t.ToString("X2"));
            }

            //返回十六进制字符串
            return lower ? sb.ToString().ToLower() : sb.ToString().ToUpper();
        }
        /// <summary>
        ///  AES 解密
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string AesDecrypt(string str, string key, string iv, CipherMode mode = CipherMode.ECB)
        {
            if (string.IsNullOrEmpty(str)) return null;
            Byte[] toEncryptArray = Convert.FromBase64String(str);
            Byte[] newkey = Convert.FromBase64String(key);
            Byte[] newiv = Convert.FromBase64String(iv);

            System.Security.Cryptography.RijndaelManaged rm = new System.Security.Cryptography.RijndaelManaged
            {
                Key = newkey,
                Mode = mode,
                Padding = System.Security.Cryptography.PaddingMode.PKCS7,
                IV = newiv,
            };

            System.Security.Cryptography.ICryptoTransform cTransform = rm.CreateDecryptor();
            Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Encoding.UTF8.GetString(resultArray);
        }
        /// <summary>
        ///  AES 解密
        /// </summary>
        /// <param name="str"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string AesDecrypt(string str, string key, CipherMode mode = CipherMode.ECB)
        {
            if (string.IsNullOrEmpty(str)) return null;
            Byte[] toEncryptArray = Convert.FromBase64String(str);

            System.Security.Cryptography.RijndaelManaged rm = new System.Security.Cryptography.RijndaelManaged
            {
                Key = Encoding.UTF8.GetBytes(key),
                Mode = mode,
                Padding = System.Security.Cryptography.PaddingMode.PKCS7,
            };

            System.Security.Cryptography.ICryptoTransform cTransform = rm.CreateDecryptor();
            Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Encoding.UTF8.GetString(resultArray);
        }
    }
}
