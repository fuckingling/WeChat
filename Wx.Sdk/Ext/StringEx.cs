﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Ext
*文件名： StringEx
*创建人： kingling
*创建时间：2018/10/1 14:19:15
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/1 14:19:15
*修改人：kingling
*描述：
************************************************************************/

namespace System
{
    public static class StringEx
    {
        public static bool IsNull(this string input)
        {
            return string.IsNullOrWhiteSpace(input);
        }
    }
}
