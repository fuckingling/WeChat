﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;

namespace Wx.Sdk.Cache
{
    internal class DefaultWxCache : IWxCache
    {
        private IMemoryCache _cache = new MemoryCache(new MemoryCacheOptions());

        public void Remove(string key)
        {
            Contract.Assume(!key.IsNull());
            _cache.Remove(key);
        }

        T IWxCache.Get<T>(string key)
        {
            Contract.Assume(!key.IsNull());
            return _cache.Get<T>(key);
        }

        void IWxCache.Set(string key, object content, int time)
        {
            Contract.Assume(!key.IsNull() && content != null);
            _cache.Set(key, content, TimeSpan.FromSeconds(time));
        }
    }
}
