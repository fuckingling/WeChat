﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Src
*文件名： IPayOrder
*创建人： kingling
*创建时间：2018/10/6 17:34:04
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 17:34:04
*修改人：kingling
*描述：
************************************************************************/

using System.Threading.Tasks;
using Wx.Sdk.Pay.Entity;

namespace Wx.Sdk.Pay.Src
{
    public interface IPayOrder
    {
        /// <summary>
        /// 扫码支付
        /// </summary>
        /// <param name="orderMoney">订单金额 单位元</param>
        /// <param name="orderNo">商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|* 且在同一个商户号下唯一</param>
        /// <param name="body">简单描述</param>
        /// <param name="productId">此参数为二维码中包含的商品ID</param>
        /// <returns>返回code_url</returns>
        Task<string> QRCodePay(decimal orderMoney, string orderNo, string body,string productId);
        /// <summary>
        /// H5支付
        /// </summary>
        /// <param name="orderMoney">订单金额 单位元</param>
        /// <param name="orderNo">商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|* 且在同一个商户号下唯一</param>
        /// <param name="body">简单描述</param>
        /// <param name="spbill_create_ip">客户端真实 必传IP</param>
        /// <returns></returns>
        Task<string> H5Pay(decimal orderMoney, string orderNo, string body, string spbill_create_ip);
        /// <summary>
        /// JSAPI所需求的一些参数
        /// </summary>
        /// <param name="orderMoney">订单金额 单位元</param>
        /// <param name="orderNo">商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|* 且在同一个商户号下唯一</param>
        /// <param name="body">简单描述</param>
        /// <param name="openid">下单用户openid</param>
        /// <returns></returns>
        Task<JsPayInfo> JsPay(decimal orderMoney, string orderNo, string body,string openid);
        /// <summary>
        /// APP支付需求的一些参数
        /// </summary>
        /// <param name="orderMoney">订单金额 单位元</param>
        /// <param name="orderNo">商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|* 且在同一个商户号下唯一</param>
        /// <param name="body">简单描述</param>
        /// <returns></returns>
        Task<AppPayInfo> AppPay(decimal orderMoney, string orderNo, string body);
        /// <summary>
        /// 退款
        /// </summary>
        /// <param name="orderNo">退款单号</param>
        /// <param name="tradeNo">发生交易的单号</param>
        /// <param name="totalMoney">总金额 元</param>
        /// <param name="refundMoney">退款金额 元</param>
        /// <returns></returns>
        Task<bool> Refund(string orderNo, string tradeNo, decimal refundMoney, decimal totalMoney);
        /// <summary>
        /// 用于企业向微信用户个人付款 
        /// </summary>
        /// <param name="orderNo">商户订单号，需保持唯一性(只能是字母或者数字，不能包含有其他字符)</param>
        /// <param name="openid">商户appid下，某用户的openid</param>
        /// <param name="amount">付款金额 单位元</param>
        /// <param name="remark">备注</param>
        /// <param name="checkName">是否验证真实姓名</param>
        /// <param name="userName">收款方真实姓名</param>
        /// <returns></returns>
        Task<bool> Payment(string orderNo, string openid, decimal amount, string remark, bool checkName, string userName = "");
        /// <summary>
        /// 普通红包
        /// </summary>
        /// <param name="orderNo">商户订单号，需保持唯一性(只能是字母或者数字，不能包含有其他字符)</param>
        /// <param name="openid">商户appid下，某用户的openid</param>
        /// <param name="amount">红包金额 单位元</param>
        /// <param name="sendName">发送方名称</param>
        /// <param name="actName">活动名称</param>
        /// <param name="wishing">祝福</param>
        /// <param name="remark">备注</param>
        /// <param name="redPack">红包类别</param>
        /// <param name="consumeMchId">资金授权商户号 服务商替特约商户发放时使用 可不填</param>
        /// <returns></returns>
        Task<bool> NormalRedPark(string orderNo, string openid, decimal amount, string sendName, string actName, string wishing, string remark, RedPackEnum redPack,string consumeMchId="");
        /// <summary>
        /// 裂变红包
        /// </summary>
        /// <param name="orderNo">商户订单号，需保持唯一性(只能是字母或者数字，不能包含有其他字符)</param>
        /// <param name="openid">商户appid下，某用户的openid</param>
        /// <param name="amount">红包金额 单位元</param>
        /// <param name="totalNum">发放人数</param>
        /// <param name="sendName">发送方名称</param>
        /// <param name="actName">活动名称</param>
        /// <param name="wishing">祝福</param>
        /// <param name="remark">备注</param>
        /// <param name="redPack">红包类别</param>
        /// <param name="consumeMchId">资金授权商户号 服务商替特约商户发放时使用 可不填</param>
        /// <returns></returns>
        Task<bool> GroupRedPark(string orderNo, string openid, decimal amount, int totalNum, string sendName, string actName, string wishing, string remark, RedPackEnum redPack, string consumeMchId);
    }
}
