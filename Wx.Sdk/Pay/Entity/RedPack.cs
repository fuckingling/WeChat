﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Entity
*文件名： Normal
*创建人： kingling
*创建时间：2018/11/16 12:22:41
*描述
*=======================================================================
*修改标记
*修改时间：2018/11/16 12:22:41
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Pay.Entity
{
    /// <summary>
    /// 红包
    /// </summary>
    internal class RedPack
    {
        /// <summary>
        /// 随机字符串，不长于32位
        /// </summary>
        public string nonce_str { get; set; }
        /// <summary>
        /// 签名  貌似也只有MD5
        /// </summary>
        public string sign { get; set; }
        /// <summary>
        /// 商户订单号 28位！！！  不合群啊（每个订单号必须唯一。取值范围：0~9，a~z，A~Z）接口根据商户订单号支持重入，如出现超时可再调用。
        /// </summary>
        public string mch_billno { get; set; }
        /// <summary>
        /// 微信支付分配的商户号
        /// </summary>
        public string mch_id { get; set; }
        /// <summary>
        /// 微信分配的公众账号ID（企业号corpid即为此appId）。在微信开放平台（open.weixin.qq.com）申请的移动应用appid无法使用该接口。
        /// </summary>
        public string wxappid { get; set; }
        /// <summary>
        /// 红包发送者名称
        /// </summary>
        public string send_name { get; set; }
        /// <summary>
        /// 接受红包的用户openid
        /// </summary>
        public string re_openid { get; set; }
        /// <summary>
        /// 付款金额，单位分
        /// </summary>
        public int total_amount { get; set; }
        /// <summary>
        /// 红包发放总人数total_num=1普通红包传1
        /// </summary>
        public int total_num { get; set; }
        /// <summary>
        /// 祝福语
        /// </summary>
        public string wishing { get; set; }
        /// <summary>
        /// 客户端IP
        /// </summary>
        public string client_ip { get; set; }
        /// <summary>
        /// 活动名
        /// </summary>
        public string act_name { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// 发放红包使用场景，红包金额大于200或者小于1元时必传
        /// PRODUCT_1:商品促销
        /// PRODUCT_2:抽奖
        /// PRODUCT_3:虚拟物品兑奖
        /// PRODUCT_4:企业内部福利
        /// PRODUCT_5:渠道分润
        /// PRODUCT_6:保险回馈
        /// PRODUCT_7:彩票派奖
        /// PRODUCT_8:税务刮奖
        /// </summary>
        public string scene_id { get; set; }
        /// <summary>
        /// 活动信息 可不填
        /// posttime:用户操作的时间戳
        /// mobile:业务系统账号的手机号，国家代码-手机号。不需要+号
        /// deviceid :mac 地址或者设备唯一标识
        /// clientversion :用户操作的客户端版本
        /// 把值为非空的信息用key = value进行拼接，再进行urlencode
        /// urlencode(posttime=xx&mobile=xx&deviceid=xx)
        /// </summary>
        public string risk_info { get; set; }
        /// <summary>
        /// 资金授权商户号 服务商替特约商户发放时使用  可不填
        /// </summary>
        public string consume_mch_id { get; set; }
    }
}
