﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Entity
*文件名： PayMsg
*创建人： kingling
*创建时间：2018/10/6 19:53:19
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 19:53:19
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Pay.Entity
{
    internal class PayMsg
    {
        public PayMsg()
        {
            this.return_code = "FAIL";
            this.return_msg = "消息通知失败";
        }
        /// <summary>
        /// 成功：SUCCESS，失败：FAIL
        /// </summary>
        public string return_code { get; set; }
        /// <summary>
        /// 成功：OK,失败：理由
        /// </summary>
        public string return_msg { get; set; }
    }
}
