﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Entity
*文件名： EnterprisePayment
*创建人： kingling
*创建时间：2018/11/16 11:07:14
*描述
*=======================================================================
*修改标记
*修改时间：2018/11/16 11:07:14
*修改人：kingling
*描述：
************************************************************************/
namespace Wx.Sdk.Pay.Entity
{
    internal class Payment
    {
        /// <summary>
        /// 微信分配的公众账号ID（企业号corpid即为此appId）
        /// </summary>
        public string mch_appid { get; set; }
        /// <summary>
        /// 微信支付分配的商户号
        /// </summary>
        public string mchid { get; set; }
        /// <summary>
        /// 微信支付分配的终端设备号 非必填
        /// </summary>
        public string device_info { get; set; }
        /// <summary>
        /// 随机字符串，不长于32位。
        /// </summary>
        public string nonce_str { get; set; }
        /// <summary>
        /// 签名 https://pay.weixin.qq.com/wiki/doc/api/tools/mch_pay.php?chapter=4_3 貌似只有MD5 实习生？
        /// </summary>
        public string sign { get; set; }
        ///// <summary>
        ///// 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
        ///// </summary>
        //public string sign_type { get; set; }
        /// <summary>
        /// 商户订单号，需保持唯一性(只能是字母或者数字，不能包含有其他字符)
        /// </summary>
        public string partner_trade_no { get; set; }
        /// <summary>
        /// 商户appid下，某用户的openid
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// NO_CHECK：不校验真实姓名 FORCE_CHECK：强校验真实姓名
        /// </summary>
        public string check_name { get; set; }
        /// <summary>
        /// 收款用户真实姓名。 如果check_name设置为FORCE_CHECK，则必填用户真实姓名
        /// </summary>
        public string re_user_name { get; set; }
        /// <summary>
        /// 企业付款金额，单位为分
        /// </summary>
        public int amount { get; set; }
        /// <summary>
        /// 企业付款备注，必填。
        /// </summary>
        public string desc { get; set; }
        /// <summary>
        /// 该IP同在商户平台设置的IP白名单中的IP没有关联，该IP可传用户端或者服务端的IP。
        /// </summary>
        public string spbill_create_ip { get; set; }
    }
}
