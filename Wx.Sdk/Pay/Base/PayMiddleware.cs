﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay
*文件名： Middleware
*创建人： kingling
*创建时间：2018/10/6 20:04:13
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 20:04:13
*修改人：kingling
*描述：
************************************************************************/

using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Wx.Sdk.Pay.Src;

namespace Wx.Sdk.Pay.Base
{
    public class PayMiddleware
    {
        private RequestDelegate next = null;
        public PayMiddleware(RequestDelegate next)
        {
            this.next = next;
        }
        public async Task Invoke(HttpContext context, WxPay wxPay, INotify notify)
        {
            if (context.Request.Path == wxPay.payOption.pay_notify)
            {
                await context.Response.WriteAsync(notify.Pay());
                return;
            }
            if (context.Request.Path == wxPay.payOption.refund_notify)
            {
                await context.Response.WriteAsync(notify.Refund());
                return;
            }
            if (next != null) await next.Invoke(context);
        }
    }
}
