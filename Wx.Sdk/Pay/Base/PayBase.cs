﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Pay.Base
*文件名： PayBase
*创建人： kingling
*创建时间：2018/10/6 18:06:59
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/6 18:06:59
*修改人：kingling
*描述：
************************************************************************/

using System;
using System.Diagnostics.Contracts;
using System.Net.Http;
using System.Threading.Tasks;

namespace Wx.Sdk.Pay.Base
{
    internal class PayBase
    {
        WxPay wxPay;
        internal PayBase(WxPay wxPay)
        {
            this.wxPay = wxPay;
        }
        /// <summary>
        /// POST数据
        /// </summary>
        /// <param name="body"></param>
        /// <param name="url"></param>
        /// <param name="takeCrt">是否带证书</param>
        /// <returns></returns>
        public async Task<string> Post(string body, string url, bool takeCrt = false)
        {
            Contract.Assume(!url.IsNull());
            var client = takeCrt ? wxPay.httpClientFactory.CreateClient("crt") : wxPay.httpClientFactory.CreateClient();
            var content = new StringContent(body);
            var responseMsg = await client.PostAsync(url, content);
            var data = await responseMsg.Content.ReadAsStringAsync();
            return data;
        }
    }
}
