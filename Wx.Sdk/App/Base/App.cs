﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Wx.Sdk.App;
using Wx.Sdk.App.Options;
using Wx.Sdk.Cache;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class App
    {
        /// <summary>
        /// 小程序注入
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddWeChatApp(this IServiceCollection services, Action<AppOption> action)
        {
            var opt = new AppOption();
            action.Invoke(opt);
            services.AddSingleton(opt);
            services.AddHttpClient();
            services.TryAddSingleton<WxApp>();
            services.TryAddSingleton<IWxCache, DefaultWxCache>();
            return services;
        }
        /// <summary>
        /// 小程序注入 需要实现IWxCache
        /// </summary>
        /// <typeparam name="TCache">IWxCache</typeparam>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddWeChatApp<TCache>(this IServiceCollection services, Action<AppOption> action) where TCache : class, IWxCache
        {
            services.AddWeChatApp(action);
            services.RemoveAll<IWxCache>();
            services.TryAddSingleton<IWxCache, TCache>();
            return services;
        }
    }
}
