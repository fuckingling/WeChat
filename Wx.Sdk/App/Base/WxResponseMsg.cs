﻿using System;
namespace Wx.Sdk.App.Base
{
    public class WxResponseMsg
    {
        /// <summary>
        /// 错误代码
        /// </summary>
        public int Errcode { get; set; }
        /// <summary>
        /// 错误消息
        /// </summary>
        public string Errmsg { get; set; }
    }
}

