﻿using System;
using Wx.Sdk.App.Base;

namespace Wx.Sdk.App.Entity
{
    public class AccessToken : WxResponseMsg
    {
        /// <summary>
        /// 调用凭证,注意：此access_token与基础支持的access_token不同
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// access_token接口调用凭证超时时间，单位（秒）目前是7200之内的值
        /// </summary>
        public int expires_in { get; set; }
    }
}

