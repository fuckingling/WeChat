﻿using System;
using Wx.Sdk.App.Base;

namespace Wx.Sdk.App.Entity
{
    /// <summary>
    /// 用户手机号
    /// </summary>
    public class PhoneNumber : WxResponseMsg
    {
        /// <summary>
        /// 手机号信息
        /// </summary>
        public PhoneInfo Phone_info { get; set; }

    }
    public class PhoneInfo
    {
        /// <summary>
        /// 用户绑定的手机号（国外手机号会有区号）
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// 没有区号的手机号
        /// </summary>
        public string PurePhoneNumber { get; set; }
        /// <summary>
        /// 区号
        /// </summary>
        public string CountryCode { get; set; }
        /// <summary>
        /// 水印
        /// </summary>
        public WaterMark WaterMark { get; set; }
    }
    public class WaterMark
    {
        /// <summary>
        /// 用户获取手机号操作的时间戳
        /// </summary>
        public long Timestamp { get; set; }
        /// <summary>
        /// 小程序Appid
        /// </summary>
        public string Appid { get; set; }
    }
}

