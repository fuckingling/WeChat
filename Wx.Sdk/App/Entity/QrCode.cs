﻿using System;
using Wx.Sdk.App.Base;

namespace Wx.Sdk.App.Entity
{
    public class QrCode : WxResponseMsg
    {
        /// <summary>
        /// 数据类型 (MIME Type)
        /// </summary>
        public string contentType { get; set; }
        /// <summary>
        /// 二维码Buff
        /// </summary>
        public string buffer { get; set; }
    }
}

