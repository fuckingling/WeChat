﻿using System.Threading.Tasks;
using Wx.Sdk.App.Entity;

namespace Wx.Sdk.App.Src
{
    public interface IAppLogin
    {
        /// <summary>
        /// 根据客户端回传的code请求解密Key
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<SessionKeyInfo> GetSessionKey(string code);
        /// <summary>
        /// 将客户端的加密数据解析成小程序微信用户
        /// </summary>
        /// <param name="rawData"></param>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        AppUser DeCodeUserInfo(RawData rawData,string sessionKey);
        /// <summary>
        /// 获取用户手机号
        /// </summary>
        /// <param name="code">code换取用户手机号。 每个 code 只能使用一次，code的有效期为5min</param>
        /// <returns></returns>
        Task<PhoneInfo> GetUserPhone(string code);
    }
}
