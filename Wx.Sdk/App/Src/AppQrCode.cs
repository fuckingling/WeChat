﻿using System;
using System.Text;
using System.Threading.Tasks;
using Wx.Sdk.App.Entity;
using Wx.Sdk.Utils;

namespace Wx.Sdk.App.Src
{
	public class AppQrCode: IAppQrCode
	{
        WxApp wxApp;
        public AppQrCode(WxApp wxApp)
        {
            this.wxApp = wxApp;
        }
        async Task<byte[]> IAppQrCode.GetWxaCodeUnlimit(AppPageInfo pageInfo)
        {
            var token = await wxApp.appBase.GetAccessToken();
            var url = $"https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token={token}";
            var body = JsonHelper.ToJson(pageInfo);
            var bytes= await wxApp.appBase.PostByte(body,url);
            return bytes;
        }
    }
}

