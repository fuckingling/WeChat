﻿using System;
using System.Threading.Tasks;
using Wx.Sdk.App.Entity;

namespace Wx.Sdk.App.Src
{
	public interface IAppQrCode
	{
		/// <summary>
        /// 小程序二维码生成 不限数量
        /// </summary>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
		Task<byte[]> GetWxaCodeUnlimit(AppPageInfo pageInfo);
	}
}

