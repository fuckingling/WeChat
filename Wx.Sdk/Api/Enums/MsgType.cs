﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Enums
*文件名： EmsgType
*创建人： kingling
*创建时间：2018/9/29 13:02:45
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 13:02:45
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Enums
{
    /// <summary>
    /// 消息总类型
    /// </summary>
    internal enum MsgType
    {
        /// <summary>
        /// 文本消息
        /// </summary>
        text = 0,

        /// <summary>
        /// 图片消息
        /// </summary>
        image,

        /// <summary>
        /// 语音消息
        /// </summary>
        voice,

        /// <summary>
        /// 视频消息
        /// </summary>
        video,

        /// <summary>
        /// 小视频消息
        /// </summary>
        shortvideo,

        /// <summary>
        /// 地理位置消息
        /// </summary>
        location,

        /// <summary>
        /// 链接消息
        /// </summary>
        link,

        /// <summary>
        /// 事件消息
        /// </summary>
        @event
    }
}
