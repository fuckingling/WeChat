﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Replys
*文件名： Video
*创建人： kingling
*创建时间：2018/9/29 20:02:12
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 20:02:12
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Entity.Replys
{
    /// <summary>
    /// 视频实体
    /// </summary>
    public class Video
    {
        /// <summary>
        /// 通过上传多媒体文件，得到的id。
        /// </summary>
        public string MediaId { get; set; }
        /// <summary>
        /// 视频消息的标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 视频消息的描述
        /// </summary>
        public string Description { get; set; }
    }
}
