﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Replys
*文件名： Article
*创建人： kingling
*创建时间：2018/9/29 19:16:04
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 19:16:04
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Entity.Replys
{
    /// <summary>
    /// 图文消息实体
    /// </summary>
    public class Article
    {
        /// <summary>
        /// 图文消息标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 图文消息描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200
        /// </summary>
        public string PicUrl { get; set; }
        /// <summary>
        /// 点击图文消息跳转链接
        /// </summary>
        public string Url { get; set; }
    }
}
