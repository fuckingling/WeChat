﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Entity.Template
*文件名： Miniprogram
*创建人： kingling
*创建时间：2018/10/5 18:14:49
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/5 18:14:49
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Entity.Template
{
    public class Miniprogram
    {
        public Miniprogram(string appid, string pagepath)
        {
            this.appid = appid;
            this.pagepath = pagepath;
        }
        public string appid { get; set; }
        public string pagepath { get; set; }
    }
}
