﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Template
*文件名： TemplateMsg
*创建人： kingling
*创建时间：2018/10/5 18:02:01
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/5 18:02:01
*修改人：kingling
*描述：
************************************************************************/

using System.Text;

namespace Wx.Sdk.Api.Entity.Template
{
    internal class TemplateMsg
    {
        public TemplateMsg(string touser, string template_id, Keyword first, Keyword remark, string url = "", Miniprogram miniprogram = null, params Keyword[] keywords)
        {
            this.touser = touser;
            this.template_id = template_id;
            this.url = url;
            this.miniprogram = miniprogram;
            StringBuilder sb = new StringBuilder();
            sb.Append(@"{" +
                "\"first\":{" +
                "\"value\":\"" + first.value + "\"," +
                "\"color\":\"" + first.color + "\"}");
            for (var i = 0; i < keywords.Length; i++)
            {
                sb.Append(@"," +
                    "\"keyword" + (i + 1).ToString() + "\":{" +
                    "\"value\":\"" + keywords[i].value + "\"," +
                    "\"color\":\"" + keywords[i].color + "\"}");
            }
            sb.Append(@"," +
                "\"remark\":{" +
                "\"value\":\"" + remark.value + "\"," +
                "\"color\":\"" + remark.color + "\"}" +
                "}");
            this.data = sb.ToString();
        }
        public string touser { get; set; }
        public string template_id { get; set; }
        public string url { get; set; }
        public Miniprogram miniprogram { get; set; }
        public string data { get; set; }
    }
}
