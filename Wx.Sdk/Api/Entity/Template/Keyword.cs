﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： Keyword
*创建人： kingling
*创建时间：2018/10/5 17:57:49
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/5 17:57:49
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Entity.Template
{
    public class Keyword
    {
        public Keyword(string value, string color) {
            this.value = value;
            this.color = color;
        }
        public Keyword(string value)
        {
            this.value = value;
            this.color = "#888888";
        }
        public string value { get; set; }
        public string color { get; set; }
    }
}
