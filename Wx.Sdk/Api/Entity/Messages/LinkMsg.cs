﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： LinkMsg
*创建人： kingling
*创建时间：2018/9/29 15:23:38
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:23:38
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 接收链接消息 MsgType=link
    /// </summary>
    public class LinkMsg : MsgBase
    {
        /// <summary>
        /// 消息标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 消息描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 消息链接
        /// </summary>
        public uint Url { get; set; }
    }
}
