﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： ImageMsg
*创建人： kingling
*创建时间：2018/9/29 15:17:54
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:17:54
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 接收图片消息 MsgType=image
    /// </summary>
    public class ImageMsg : MsgBase
    {
        /// <summary>
        /// 图片链接
        /// </summary>
        public string PicUrl { get; set; }
        /// <summary>
        /// 图片消息媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string MediaId { get; set; }
    }
}
