﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： TextMsg
*创建人： kingling
*创建时间：2018/9/29 15:14:22
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:14:22
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 接收文本消息 MsgType=text
    /// </summary>
    public class TextMsg:MsgBase
    {
        /// <summary>
        /// 文本内容
        /// </summary>
        public string Content { get; set; }
    }
}
