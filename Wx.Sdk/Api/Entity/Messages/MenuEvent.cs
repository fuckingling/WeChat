﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： MenuEvent
*创建人： kingling
*创建时间：2018/9/29 15:36:18
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:36:18
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 自定义菜单事件,事件类型，CLICK,	点击菜单跳转链接时的事件推送点击事件类型，VIEW
    /// </summary>
    public class MenuEvent : EventBase
    {
        /// <summary>
        /// 事件KEY值，与自定义菜单接口中KEY值对应，如果是点的菜单上的链接则KEY为设置的跳转URL
        /// </summary>
        public string EventKey { get; set; }
    }
}
