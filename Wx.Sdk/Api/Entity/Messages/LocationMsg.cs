﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： LocationMsg
*创建人： kingling
*创建时间：2018/9/29 15:22:53
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:22:53
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 接收位置消息 MsgType=location
    /// </summary>
    public class LocationMsg : MsgBase
    {
        /// <summary>
        /// 地理位置维度
        /// </summary>
        public decimal Location_X { get; set; }
        /// <summary>
        /// 地理位置经度
        /// </summary>
        public decimal Location_Y { get; set; }
        /// <summary>
        /// 地图缩放大小
        /// </summary>
        public uint Scale { get; set; }
        /// <summary>
        /// 地理位置信息
        /// </summary>
        public string Label { get; set; }
    }
}
