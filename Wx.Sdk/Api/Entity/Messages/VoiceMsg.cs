﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： VoiceMsg
*创建人： kingling
*创建时间：2018/9/29 15:19:56
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:19:56
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 接收语音消息 MsgType=voice
    /// </summary>
    public class VoiceMsg : MsgBase
    {
        /// <summary>
        /// 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
        /// </summary>
        public string MediaId { get; set; }
        /// <summary>
        /// 语音格式，如amr，speex等
        /// </summary>
        public string Format { get; set; }
        /// <summary>
        /// 开启语音识别后会多这么个东西,应该是语音识别内容
        /// </summary>
        public string Recognition { get; set; }
    }
}
