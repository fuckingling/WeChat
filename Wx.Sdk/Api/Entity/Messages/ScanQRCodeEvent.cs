﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： Class1
*创建人： ScanQRCodeEvent
*创建时间：2018/9/29 15:25:23
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:25:23
*修改人：kingling
*描述：
************************************************************************/
using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 扫描带参数二维码事件:1. 用户未关注时，进行关注后的事件推送,事件类型，subscribe.2. 用户已关注时的事件推送,事件类型，SCAN
    /// </summary>
    public class ScanQRCodeEvent : EventBase
    {
        /// <summary>
        /// 事件KEY值，qrscene_为前缀，后面为二维码的参数值
        /// </summary>
        public string EventKey { get; set; }
        /// <summary>
        /// 二维码的ticket，可用来换取二维码图片
        /// </summary>
        public string Ticket { get; set; }
    }
}
