﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages
*文件名： TemplateMsgEvent
*创建人： kingling
*创建时间：2018/9/29 17:45:45
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 17:45:45
*修改人：kingling
*描述：
************************************************************************/

using Wx.Sdk.Api.Entity.Base;

namespace Wx.Sdk.Api.Entity.Messages
{
    /// <summary>
    /// 模板消息发送结束
    /// </summary>
    public class TemplateMsgEvent: EventBase
    {
        /// <summary>
        /// 消息ID
        /// </summary>
        public ulong MsgID { get; set; }
        /// <summary>
        /// 消息状态 success  failed:user block
        /// </summary>
        public string Status { get; set; }
    }
}
