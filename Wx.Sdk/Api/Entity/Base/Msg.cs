﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Messages.Base
*文件名： Msg
*创建人： kingling
*创建时间：2018/9/29 15:07:44
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 15:07:44
*修改人：kingling
*描述：
************************************************************************/


using Wx.Sdk.Utils;

namespace Wx.Sdk.Api.Entity.Base
{
    public class Msg
    {
        public Msg() {
            CreateTime = TimeHelper.ToUnix();
        }
        /// <summary>
        /// 开发者微信号
        /// </summary>
        public string ToUserName { get; set; }
        /// <summary>
        /// 发送方帐号（一个OpenID）
        /// </summary>
        public string FromUserName { get; set; }
        /// <summary>
        /// 消息创建时间 （整型）
        /// </summary>
        public int CreateTime { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        public string MsgType { get; set; }
    }
}
