﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Entity
*文件名： JsSdk
*创建人： kingling
*创建时间：2018/10/1 14:10:36
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/1 14:10:36
*修改人：kingling
*描述：
************************************************************************/

namespace Wx.Sdk.Api.Entity
{
    public class JsSdk
    {
        /// <summary>
        /// appID
        /// </summary>
        public string appId { get; set; }
        /// <summary>
        /// 时间戳
        /// </summary>
        public int timestamp { get; set; }
        /// <summary>
        /// 随即字符串
        /// </summary>
        public string nonceStr { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string signature { get; set; }
    }
}
