﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Src
*文件名： IAuth
*创建人： kingling
*创建时间：2018/10/1 14:08:15
*描述
*=======================================================================
*修改标记
*修改时间：2018/10/1 14:08:15
*修改人：kingling
*描述：
************************************************************************/

using System.Threading.Tasks;
using Wx.Sdk.Api.Entity;
using Wx.Sdk.Api.Entity.Template;

namespace Wx.Sdk.Api.Src
{
    public interface IApiService
    {
        /// <summary>
        /// 网页授权
        /// </summary>
        /// <param name="url">授权回跳地址</param>
        /// <param name="state">授权回传参数</param>
        /// <param name="agree">是否弹出用户提示框</param>
        void Auth(string url, string state = "", bool agree = true);
        /// <summary>
        /// 根据URL算出wx.config的几个参数
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        JsSdk GetJsSdkConfig(string url);
        /// <summary>
        /// 带参数二维码图片地址
        /// </summary>
        /// <param name="scene_id">数字</param>
        /// <param name="expire_seconds">有效秒数 0为永久  最大不超过2592000（即30天）</param>
        /// <returns></returns>
        Task<string> GetQRcodeImgSrc(int scene_id, int expire_seconds = 7200);
        /// <summary>
        /// 带参数二维码图片地址
        /// </summary>
        /// <param name="scene_str">字符串</param>
        /// <param name="expire_seconds">有效秒数 0为永久  最大不超过2592000（即30天）</param>
        /// <returns></returns>
        Task<string> GetQRcodeImgSrc(string scene_str, int expire_seconds = 7200);
        /// <summary>
        /// 授权用户获得信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<WxUser> GetUserWithAuth(string code);
        /// <summary>
        /// 授权只获取openid，用于静默授权
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<string> GetOpenidWithAuth(string code);
        /// <summary>
        /// 发送模板消息
        /// </summary>
        /// <param name="touser">接收用户</param>
        /// <param name="template_id">模板ID</param>
        /// <param name="first">标题</param>
        /// <param name="remark">备注</param>
        /// <param name="url">跳转地址 非必填</param>
        /// <param name="miniprogram">跳转小程序 非必填  url和mini都传 会优先跳转至小程序 </param>
        /// <param name="keywords">关键字</param>
        /// <returns></returns>
        Task<bool> SendTemplateMsg(string touser, string template_id, Keyword first, Keyword remark, string url = "", Miniprogram miniprogram = null, params Keyword[] keywords);
    }
}
