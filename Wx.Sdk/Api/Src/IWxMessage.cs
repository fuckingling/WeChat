﻿/************************************************************************
* Copyright (c) 2018 All Rights Reserved.
*命名空间：Wx.Sdk.Api.Src
*文件名： IMessage
*创建人： kingling
*创建时间：2018/9/29 13:25:43
*描述
*=======================================================================
*修改标记
*修改时间：2018/9/29 13:25:43
*修改人：kingling
*描述：
************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Wx.Sdk.Api.Src
{
    public interface IWxMessage
    {
        /// <summary>
        /// 当有微信消息时
        /// </summary>
        /// <returns></returns>
        Task<string> HaveMsg();
    }
}
